import { createPreloader, deletePreloader, sendSuccess } from '../interface/preloader'


/**
 * Отправка запроса на изменение данных пользователя личного кабинета.
 * @param {EVENTObject} event Объект события отправки формы
 */
function changeAccountData(event) {
    event.preventDefault()

    let form = event.target
    let data = $(form).serialize()
    let action = form.action
    let method = form.method

    $(form).find('input').removeClass('error')
    $(form).find('textarea').removeClass('error')

    $.ajax({
        url: action,
        type: method,
        dataType: 'json',
        data: data,
        success: function(response) {
            console.log(response)
            if(response.errors !== undefined && !response.errors) {
                console.log(response.message)
            } else {
                if(response.errors !== undefined) {
                    for(let item in response.fields) {
                        let input = form.querySelector(`[name="${ item }"]`)
                        input.closest('.form_group').classList.add('error')
                    }
                }
            }
        },
        error: function() {
            console.log('error')
        }
    })
}

/**
 * Отправка запроса на добавление пользоавтеля личного кабинета в подписчики.
 * @param {EVENTObject} event Объект события отправки формы
 */
function addAccountSubscribe(event) {
    event.preventDefault()
    let csrf = document.querySelector('.csrf_token input')
    let button = event.currentTarget
    let action = button.dataset.action
    let data = `${ csrf.name }=${ csrf.value }`

    $.ajax({
        url: action,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function(response) {
            console.log(response)
            button.innerText = response.message
        },
        error: function(error) {
            console.log(error)
        }
    })
}

/**
 * Отправка запроса на смену пароля пользователя Django к которому првязан
 * пользователь личного кабинета.
 * @param {EVENTObject} event Объект события отправки формы
 */
function changeAccountPassword(event) {
    event.preventDefault()

    let form = event.target
    let data = $(form).serialize()
    let action = form.action
    let method = form.method

    $(form).find('input').removeClass('error')
    $(form).find('textarea').removeClass('error')

    createPreloader('.change-password-form')

    $.ajax({
        url: action,
        type: method,
        dataType: 'json',
        data: data,
        success: function(response) {
            console.log(response)
            deletePreloader('.change-password-form')
            if(response.errors !== undefined && response.errors) {
                for(let item in response.fields) {
                    let input = form.querySelector(`[name="${ item }"]`)
                    input.closest('.form_group').classList.add('error')
                }
            } else {
                console.log(response.message)
                setTimeout(() => {
                    sendSuccess('.change-password-form', 'Пароль успешно изменен')
                }, 150)
            }
        },
        error: function(error) {
            console.log(error)
        }
    })
}

/**
 * Авторизация в личном кабинете
 * @param {EVENTObject} event Объект события отправки формы
 */
function loginAccount(event) {
    event.preventDefault()

    let form = event.target
    let data = $(form).serialize()
    let action = form.action
    let method = form.method

    $(form).find('input').removeClass('error')
    $(form).find('textarea').removeClass('error')
    $(form).find('.error_description').remove()

    $.ajax({
        url: action,
        type: method,
        dataType: 'json',
        data: data,
        success: function(response) {
            console.log(response)
            if(response.errors !== undefined && response.errors) {
                for(let item in response.fields) {
                    let input = form.querySelector(`[name="${ item }"]`)
                    input.classList.add('error')
                }
                $(form).find('[type="submit"]').before(`<span class="error_description">${ response.message }</span>`)
            } else {
                sendSuccess('#custom_modal-1', '')
                setTimeout(() => {
                    document.location.href = response.redirect
                }, 300)
            }
        },
        error: function(error) {
            console.log(error)
        }
    })
}

/**
 * Регистрация в личном кабинете
 * @param {EVENTObject} event Объект события отправки формы
 */
function registerAccount(event) {
    event.preventDefault()

    let form = event.target
    let data = $(form).serialize()
    let action = form.action
    let method = form.method

    $(form).find('input').removeClass('error')
    $(form).find('.error_description').remove()

    createPreloader('#custom_modal-3')

    $.ajax({
        url: action,
        type: method,
        dataType: 'json',
        data: data,
        success: function(response) {
            console.log(response)
            deletePreloader('#custom_modal-3')
            if(response.errors !== undefined && response.errors) {
                for(let item in response.fields) {
                    let input = form.querySelector(`[name="${ item }"]`)
                    input.classList.add('error')
                }
                $(form).find('[type="submit"]').before(`<span class="error_description">${ response.message }</span>`)
            } else {
                sendSuccess('#custom_modal-3', 'Вы успешно зарегистрировались')
                setTimeout(() => {document.location.href = document.location.href}, 800)
            }
        },
        error: function(error) {
            console.log(error)
        }
    })
}


$('body').on('submit', '.account-data-change-form', changeAccountData)
$('body').on('click', '.subscribe_button', addAccountSubscribe)
$('body').on('submit', '.change-password-form', changeAccountPassword)
$('body').on('submit', '.account-login-form', loginAccount)
$('body').on('submit', '.register-form', registerAccount)
