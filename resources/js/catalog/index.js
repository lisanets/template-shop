import { importAll } from '../components/utils'


const catalog = importAll(require.context('./', false, /\.(js|jsx)$/));
