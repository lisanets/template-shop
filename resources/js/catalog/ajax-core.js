// import { initNanoScroller } from '../interface'
import { createPreloader, deletePreloader } from '../interface/preloader';


/**
 * Обработчк формы фильтрации
 * @param {object} event Форма фильтрации
 * @param {boolean} more Была ли нажата кнопка показать еще над пагинацией
 * hurhu
 */
export function updateCatalog(event, more) {
    $('.more').addClass('loading')

    let query_string = getQuery(more);
    window.history.pushState(null, null, window.location.pathname + "?" + query_string);
    query_string += "&ajax=Y";

    if(!more)
        createPreloader('body')

    $.ajax({
        url: window.location,
        type: 'GET',
        dataType: 'json',
        data: "ajax=on",
        success: function(response) {
            let $productContainer = $('.products-container')
            let $filters = $(event.target)

            if (more) {
                $productContainer.children('.row').append(response.products);
            } else {
                $productContainer.children('.row').html(response.products);
            };
            // initNanoScroller()

            $('.pagination').remove();
            $('.more').remove();
            $productContainer.after(response.pagination);
            $productContainer.after(response.more_button)

            $('.filters__count').html(response.count_products);

            deletePreloader('body')
        },
        error: function() { alert('error'); }
    });
}

/**
 * Формирование параметров для передачи в URL
 * @param {boolean} more Была ли нажата кнопка показать еще над пагинацией
 */
function getQuery(more) {
    var filters = $('.filters > form').serialize();

    let url = new URL(window.location.href)
    let search = ''
    let searchParam = url.searchParams.get('search')

    if ($('.pagination__item.current span').length)
        var page = $('.pagination__item.current span').text();
    else if ($('.pagination__item.current a').length)
        var page = $('.pagination__item.current a').attr('data-page')
    else
        var page = 1

    if (more) {
        page = $('.pagination__item.current').text();
        page++;
    }

    if(searchParam != null)
        search = '&search=' + searchParam

    return filters + '&page=' + page + search;
}


/**
 * Обработчик пагинации
 * @param {object} event
 */
function pagination(event) {
    event.preventDefault();
    var $target = $(event.target);

    var top = $('.page').offset().top;
    $('html,body').stop().animate({ scrollTop: top }, 400);

    if(event.target.tagName != "A")
        $target = $(event.target).closest("a");

    $('.pagination__item').removeClass('current');
    $target.closest('li').addClass('current');

    updateCatalog(false, false);
}


function paginationMore(event) {
    updateCatalog(false, true)
}




////////////
// EVENTS //
////////////
// Фильтры
$('.filters').on('submit', function(event) { event.preventDefault(); updateCatalog(event, false); });

// Пагинацыя
$('body').on('click', '.pagination__item a', pagination);
$('body').on('click', '.more', paginationMore)