export function createToolpopup(text, selector) {
    $('.toolpopup').remove()
    
    let toolpopup = document.createElement('div')
    toolpopup.classList.add('toolpopup')
    toolpopup.innerText = text

    document.body.appendChild(toolpopup)

    let elem = document.querySelector(selector)

    let offset = $(elem).offset()
    let offsetTop = offset.top + elem.clientHeight + 18
    let offsetLeft = offset.left - toolpopup.clientWidth / 2 + elem.clientWidth / 2

    toolpopup.style.top = `${ offsetTop }px`
    toolpopup.style.left = `${ offsetLeft }px`
    toolpopup.classList.add('toolpopup_visible')

    return toolpopup
}

export function deleteToolpopup(toolpopup) {
    toolpopup.classList.remove('toolpopup_visible')
    setTimeout(() => { toolpopup.remove() }, 150)
}
