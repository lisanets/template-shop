import { createToolpopup, deleteToolpopup } from '../interface/toolpopup';
import { createPreloader, deletePreloader } from '../interface/preloader';


/**
 * Ajax Обработчик добавления товара в корзину
 * @param {EVENTObject} event Событие отправки формы с опциями товара
 */
export function cartAdd(event) {
    event.preventDefault();

    let form = event.target;
    let data = $(form).serialize();
    let action = form.action;
    let method = form.method;
    let cart = document.querySelector('.cart-button')
    let csrf = document.querySelector('.csrf_token input')

    $.ajax({
        url: action,
        type: method,
        dataType: 'json',
        data: data + '&' + csrf.name + '=' + csrf.value,
        success: function(response) {
            console.log(response);

            if (response.errors) {
                let toolpopup = createToolpopup(response.message, '.add_to_cart')
                setTimeout(() => { deleteToolpopup(toolpopup) }, 2000)
            } else {
                $('.cart-button__count').html(response.count)
                $('.cart-dropdown').remove()
                $(cart).append(response.dropdown)
                $('.cart-dropdown').fadeIn(150).css('transform', 'translateY(0px)');
                $(window).resize()

                setTimeout(() => {
                    if(!cart.classList.contains('blocked-ajax-fadeOut'))
                        $('.cart-dropdown').fadeOut(150);
                }, 3000)
            }
        },
        error: function() {
            console.log('error');
        }
    });
}

/**
 * Ajax обработчик отправки обновленных данных элемента корзины
 * @param {HTMLFormElement} form Форма элемента корзины
 */
export function cartUpdate(event) {
    let form = event.target.closest('.cart-item')
    let data = $(form).serialize();
    let action = form.action;
    let method = form.method;
    let cart = document.querySelector('.button-cart')

    $.ajax({
        url: action,
        type: method,
        dataType: 'json',
        data: data,
        success: function(response) {
            console.log(response);

            // Обновление выпадающего меню кнопки корзины
            $('.cart-dropdown').remove()
            $('.cart-button').append(response.dropdown)

            form.querySelector('.cart-item__total-price').innerHTML = response.price;
            document.querySelector('.cart-total-price').innerHTML = response.total;
        },
        error: function() {
            console.log('error');
        }
    });
}

/**
 * Ajax обработчик удаления товара из корзины
 * @param {EVENTObject} event Объект события клика по кнопке
 */
function cartDelete(event) {
    event.preventDefault();

    let button = event.currentTarget;
    let form = button.closest('.cart-item')
    let data = $(form).serialize();
    let action = button.attributes['data-action'].value;
    let method = form.method;
    let cart = document.querySelector('.cart-button')

    $.ajax({
        url: action,
        type: method,
        dataType: 'json',
        data: data,
        success: function(response) {
            console.log(response)
            // Обновление числа количества товаров в корзине
            document.querySelector('.cart-button__count').innerHTML = response.count;

            if(response.empty !== undefined) {
                // Если товары закончились когда мы удалили товар, то мы меняем корзину
                // с товарами, на шаблон с пустой корзиной
                
                let cartWrapper = document.querySelector('.cart-wrapper');
                cartWrapper.style.opacity = 1;

                // Анимация изчезания и появления корзины со сменой на шаблон
                // пустой корзины
                $(cartWrapper).animate({
                    opacity: 0
                }, 200, () => {
                    $(cartWrapper).html(response.empty);
                    $(cartWrapper).animate({opacity: 1}, 200);
                })
            } else {
                form.style.height = form.clientHeight
                form.style.opacity = 1
                $(form).stop(true, false).animate({
                    height: 0,
                    opacity: 0
                }, 200, () => {
                    $(form).remove()
                })
            }

            // Обновление выпадающего меню кнопки корзины
            $('.cart-dropdown').remove()
            $(cart).append(response.dropdown)
            $('.cart-total-price').html(response.total)
        },
        error: function() {
            console.log('error');
        }
    });
}




$('body').on('submit', '.cart-item', (event) => {event.preventDefault();})
$('body').on('submit', '.product-form', cartAdd);
$('body').on('change', '.cart-item [name="product-count"]', cartUpdate)
$('body').on('click', '.cart-item__delete', cartDelete)