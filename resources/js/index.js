// Core
import './vendors';

import './interface';
import './account';
import './catalog';
import './shop';
import './feedback';



// Вывод времени загрузки странцы в консоль
$(window).on('load', () => {
    let loadTime = window.performance.timing.loadEventStart - window.performance.timing.domainLookupEnd;
    console.log(`Время загрузки страницы: ${ loadTime / 1000 }с`);
})
