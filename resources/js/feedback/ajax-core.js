function feedbackQuestion(event) {
    event.preventDefault();

    let form = event.target;
    let data = $(form).serialize()
    let action = form.action
    let method = form.method

    $(form).find('input').removeClass('error');
    $(form).find('textarea').removeClass('error');

    // createPreloader(form)

    $.ajax({
        url: action,
        type: method,
        dataType: 'json',
        data: data,
        success: function(response) {
            // deletePreloader(form)

            if(response.errors !== undefined && !response.errors) {
                sendSuccess(form, response.message)
            } else {
                if(response.errors !== undefined) {
                    for(let item in response.fields) {
                        let input = form.querySelector(`[name="${ item }"]`);
                        input.classList.add('error');
                    }
                }
            }
        },
        error: function() {
            console.log('error');
        }
    });
}


$('body').on('submit', '.question-form', feedbackQuestion)
