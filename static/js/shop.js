
function get_get_query(more = false) {
	var filters = $('form.filtering').serialize();
	var page = $('.pagination li.active a').attr('data-page');
	var sort = $('div.order_by select').serialize();
	if (more)
        page = $('#show_more').attr('data-page');
    
    if (!(page))
        page = 1

    
    var type_display = $('.sort_parameters').find('button.active').attr('data-type');

	return filters + '&page=' + page + '&type=' + type_display + '&' + sort;
}

//execute filtering
function updateCatalog(event) {
	event.preventDefault();
	query_string = get_get_query();
	window.history.pushState(null, null, window.location.pathname + "?" + query_string);
	query_string += "&ajax=Y";
	$.ajax({
		url: window.location.pathname,
		type: 'GET', 
		dataType: 'json',
		data: query_string,
		success: function(data) {
			loadData(event, data);
		},
		error: function() { alert('error');}
	});
}

function loadData(event, data, more = false) {
	$('.common_count').fadeOut(200);  
	
	if(data['products'].length != 0) {
		// $("div.paginate").remove();
		var html_products = data['products'];
		if (more)
			$('div.change_products').append(html_products);
		else
            $('div.change_products').html(html_products);
        // $('.products-container').append('<div class="paginate">' + data['pagination'] + '</div>');
        $('.paginationBlock').html(data['pagination']);
        
        $('.filterBlock').replaceWith(data['template_filters']);

        // range slider init
        $(".range_slider_field").each(function(i, e){
			var $range = $(e).find('.range_slider');
			$(e).find('.range_slider_inputs .min_price').val( $range.data("from") );
			$(e).find('.range_slider_inputs .max_price').val( $range.data("to") );

			$range.ionRangeSlider();
			$range.on("change", function () {
				var $this = $(this),
					value = $this.prop("value").split(";");

				$(e).find(".range_slider_inputs .min_price").val(value[0]);
				$(e).find(".range_slider_inputs .max_price").val(value[1]);
				filterShowCounter(this);
			});


			$(e).find(".range_slider_inputs input").change(function (index, elem) {
				filterShowCounter(this);

				$range_data = $(this).parents(".range_slider_field").find('.range_slider').data("ionRangeSlider");
				$range_data.update({
					from: $(e).find(".range_slider_inputs .min_price").val(),
					to: $(e).find(".range_slider_inputs .max_price").val()
				});
			});
		});
        //
	} else $('.products-container row').html('<h3>Товары по вашему запросу не найдены</h3>');
}

function loadDataNews(event, data, target) {
	if(data['objects'].length != 0) {
		var html_objects = "";
		$.each(data['objects'], function(key, value) {
			html_objects += value;
		});
		html_objects += data['pagination'];
		target.closest(".row").append(html_objects);
		target.parents(".parentMore").remove()
	}
}

//function change page 
function changePage(event) {
	event.preventDefault();
	var target = $(event.target);
	$('.pagination li').removeClass('active');
	$(target).closest('li').addClass('active');
	updateCatalog(event, false);
	$('body,html').animate({scrollTop: 0}, 400);
}

//set csrf to data
function csrf(str) {
	var input = $('.csrf_token').find('input');
	return str + "&" + $(input).attr('name') + "=" + $(input).attr('value');
}



function scroll() {
	$("body").animate({"scrollTop":400}, "slow");
}




//submit ajax forms
function submitAjaxForm(event, funcCall) {
	
	event.preventDefault();
	$('.error_message').html('');
	if(event.target.tagName == "BUTTON" || event.target.tagName == "A" || event.target.tagName == "SPAN" )
		var target = $(event.target).closest("form");
	else 
		var target = $(event.target);
	target.closest("form").addClass("load");
	var serial = $(target).serialize();
    var url = $(target).attr('action');
	var method = $(target).attr('method');

	$.ajax({
		url: url, 
		type: method,
		dataType: 'json',
		data: csrf(serial),
		success: function(data) {
            if (data['errors'])
                showErrors(event,data)
            else
                funcCall(event, data);
                target.closest("form").addClass("success");
            target.closest("form").removeClass('load'); 
		},
		error: function() { 
			alert('error');
			target.closest("form").removeClass('load'); 
		}
	});

}

// function showErrors(event, data) {
// 	var error_message = "";
// 	$.each(data['errors'], function(key, value) {
// 		if(event.target.tagName == "BUTTON" || event.target.tagName == "A")
// 			target = $(event.target).closest("form");
// 		else
// 			target = $(event.target);
// 		$(target).find('input[name=' + key +  ']').addClass('error');
// 		$(target).find('textarea[name=' + key +  ']').addClass('error');
// 		$.each(value, function(key1, value1) {
// 			error_message += value1 + "</br>";
// 		});
// 	});
// 	var err = $(target).find('.error_message');
	
// 	$(err).html(error_message);
// 	$(err).addClass('active');
// }

function showErrors(event, data) {
    if(event.target.tagName == "BUTTON" || event.target.tagName == "A")
        target = $(event.target).closest("form");
    else
        target = $(event.target);
    $(target).find('input').removeClass("is-invalid is-valid");
    $(target).find('input').addClass("is-valid");
    $(target).find('div.state').removeClass("valid-feedback invalid-feedback");
    $(target).find('div.state').text("Готово!");
    $(target).find('div.state').addClass("valid-feedback");
    
    $.each(data['fields'], function(key, value) {
        var input_error = $(target).find('input[name=' + key +  ']')
        input_error.removeClass("is-valid");
        input_error.addClass("is-invalid");
        var div_error = $(input_error.siblings('div'));
        $(div_error).addClass('invalid-feedback');
        div_error.text(value);
        $(target).find('textarea[name=' + key +  ']').addClass('error');
    });

}

function showErrorsButton(target, data) {
	var error_message = "";
	$.each(data['errors'], function(key, value) {
		$(target).find('input[name=' + key +  ']').addClass('error');
		$.each(value, function(key1, value1) {
			error_message += value1 + "</br>";
		});
	});
	var err = $(target).find('.error_message');
	
	$(err).html(error_message);
	$(err).addClass('active');
}

function offForm(form) {
	$(form).find('input').attr('disabled', true);
	//$(form).animate({opacity:0.5}, 500);
}

function onForm(form) {
	$(form).find('input').attr('disabled', false);
	//$(form).animate({opacity:1.0}, 0);
}



//call form
$('#callForm, #consForm').on('submit', function(event) {
	submitAjaxForm(event, callBackForm)
})


function callBackForm(event, data) {
	if(event.target.tagName == "BUTTON")
		target = $(event.target).closest("form");
	else
		target = $(event.target);
	onForm(target);
	$(".error_message").removeClass('active');
	if(data['error'] != 0) {
		showErrors(event, data);
		console.log('error');
	} else $(target).html('<h3 style="color:white;">Заявка отправлена. Наш менеджер свяжется с вами в ближайшее время</h3>');
}






//execute filtering
function showMore(event) {
	event.preventDefault();
	$('a.ajax_page').addClass('ajax');
	query_string = get_get_query(true);
	window.history.pushState(null, null, window.location.pathname + "?" + query_string);
	query_string += "&ajax=Y";
	$.ajax({
		url: window.location.pathname,
		type: 'GET', 
		dataType: 'json',
		data: query_string,
		success: function(data) {
			loadData(event, data, true);
		},
		error: function() { alert('error');}
	});
}



function countProducts(event) {
		// $(".tooltip_count").fadeOut();
		query_string = get_get_query();
		query_string += "&ajax=Y";
		$.ajax({
			url: window.location.pathname,
			type: 'GET', 
			dataType: 'json',
			data: query_string,
			success: function(data) {
				if (data['count']>0) 
				{
					$('.common_count span').text("Показать "+data['count']+ " " +data['word_count'])
				}
				else
				{
					$('.common_count span').text("Товаров не найдено ")
				}

			},
			error: function() { alert('error');}
		});
}


function addProductInCart(event) {
    event.preventDefault();
    var form = $(event.target).closest('form');
    var data = form.serialize();
    var url = form.attr('action');
    console.log(data);
	$.ajax({
		url: url,
		type: 'POST', 
		dataType: 'json',
		data: csrf(data),
		success: function(data) {
            $('div.order-table').replaceWith(data['dropdown']);
            $('span.cart_count').text(data['count']);
			console.log('succes');

		},
		error: function() { alert('error');}
		});
}

function deleteProduct(event) {
	event.preventDefault();
	var $target = $(event.target).closest("div.basket__card_wrapper");
	var product = $target.attr("data-id")
	$.ajax({
		url: "/api/addproduct/",
		type: 'POST', 
		dataType: 'json',
		data: csrf("type=delete&id="+product),
		success: function(data) {
			if (data['error'] == 0) {
				$target.closest('.basket__card').remove();
				$('.basket__result_price').text(data['sum'] + " руб.");
			}

		},
		error: function() { alert('error');}
		});
}


function sendCreateOrder(event) {
	event.preventDefault();
	var target = $(event.target).closest('form');
	var data = $(target).serialize();
	console.log(data);
	var url = $(target).attr('action');
	// $(target).find(".error_message").detach();
	// $(target).find(".error").removeClass("error");
	$.ajax({
		url: url,
		type: 'POST',
		dataType: 'json',
		data: csrf(data),
		success: function(data) {
			if(data['errors']) {
                target.find("input").removeClass("is-invalid");
                // target.find("input").addClass("is-valid");
                // target.find("input").siblings("div").removeClass("invalid-feedback");
                // target.find("input").siblings("div").addClass("valid-feedback");
				$.each(data['fields'], function(key, value) {
                    inp = target.find("input[name=" + key + "][type!=hidden][disabled!=disabled]").addClass('is-invalid');
                    inp.removeClass("is-valid");
                    inp.siblings("div:not(.suggestions-wrapper)").removeClass("valid-feedback");
                    inp.siblings("div:not(.suggestions-wrapper)").addClass("invalid-feedback");
                    inp.siblings("div:not(.suggestions-wrapper)").text(value);
				});
				scroll();
			} else  {
                $('.removeSuccess').remove();
                $('.orderContainer').append(data['template']);
                if (data['redirect'])
                	window.location = data['redirect'];
            }
		},
		error: function() { 
			alert('error'); }
	});
}

var AJAX_SEARCH_MUTEX = false;

function smartAjaxSearch(event) {

    var search = $.trim($(event.target).val());
    var url = $(event.target).closest('form').attr('url-api');
    if ($(event.target).closest('form').attr('data-attr') == "desc")
    	class_block = ".search-form__ajax-search"
    else
    	class_block = ".ajax_search"
    console.log(class_block);
	if(search.length == 0) { 
				AJAX_SEARCH_MUTEX = false; 
				$('#navbarNavDropdown').html('');
				 $(class_block + ' ul').slideUp(150);
				return true;
	}
	if(AJAX_SEARCH_MUTEX && AJAX_SEARCH_MUTEX != event) {
		AJAX_SEARCH_MUTEX = event;
		return true;
	} 
	AJAX_SEARCH_MUTEX = event;
	$.ajax({
		url: url,
		type: 'POST',
		dataType: 'json',
		data: csrf($(event.target).closest('form').serialize()),
		success: function(data) {
			 console.log(data)
			if(AJAX_SEARCH_MUTEX == event) { AJAX_SEARCH_MUTEX = false;}
			else { smartAjaxSearch(AJAX_SEARCH_MUTEX); return true; }
			var result_html = "";
			if(data['template']) {
				result_html = data['template']
				$(class_block).html(result_html);
				$(class_block + ' ul').slideDown();
			} else {
				$(class_block).html('');
				$(class_block + ' ul').slideUp(150);

			}
		},
		error: function() { alert('error'); }
	});
}

function resetFilter(event){
	window.location = window.location.pathname;
}






function addProductFromCart(event) {
    var form = $(event.target).closest('form');
    var data = form.serialize();
    var url = form.attr('action');



	$.ajax({
		url: url,
		type: 'POST', 
		dataType: 'json',
		data: csrf(data),
		success: function(data) {
            console.log(" -------suc------ ");
            form.find('.total-price-number').text(data['price']);
            console.log(data['total']);
            $('.cart-table__footer strong').text(data['total'] + " руб.");

		},
		error: function() { alert('error');}
		});
}

function addInAccount(event, funcCall) {
	
	if (event.target.tagName == "BUTTON")
        var target = $(event.target);
    else
        var target = $(event.target).closest('button');
    var product = target.closest("a.product_card, .wide_product_card").attr('data-product');
    var url = target.attr('data-url');

	$.ajax({
		url: url, 
		type: "POST",
		dataType: 'json',
		data: csrf('product='+product),
		success: function(data) {
			funcCall(event, data);
		},
		error: function() { 
			alert('error');
		}
	});

}

function FavoriteCallback(event,data) 
{
    $('.favorite_count').text(data['count']);
}
function CompareCallback(event,data) 
{
    $('.compare_count').text(data['count']);
}

// Функции добавления, удаления, восстановления избранных товаров
$('body').on('click', '.add_to_favorites',  function(event) {
	addInAccount(event, FavoriteCallback)
});

$('body').on('click', '.remove_product',  function(event) {
    console.log("remoooove")
	addInAccount(event, FavoriteCallback)
});

$('body').on('click', '.restore_product',  function(event) {
	addInAccount(event, FavoriteCallback)
});

$('body').on('click', '.compare',  function(event) {
	addInAccount(event, CompareCallback)
});


//Личный кабинет

function loginCallback(event,data) 
{
    if (data['reload'])
    {
        window.location = window.location;
    }
}

$('#custom_modal-1 form').on('submit', function(event) {
    event.preventDefault();
    console.log("popopp")
	submitAjaxForm(event, loginCallback)
})

$('#custom_modal-3 button').on('click', function(event) {
    event.preventDefault();
    console.log("popopp")
	submitAjaxForm(event, loginCallback)
})


$('#custom_modal-4 button').on('click', function(event) {
    event.preventDefault();
    console.log("popopp")
	submitAjaxForm(event, loginCallback)
})

//call form
$('#recall_modal button').on('click', function(event) {
	submitAjaxForm(event, callBackForm)
})






function delProductFromCart(event) {
	var form = $(event.target).closest('form');
    var data = form.serialize();
	$.ajax({
		url: "/api/shop/cart/delete/",
		type: 'POST', 
		dataType: 'json',
		data: csrf(data),
		success: function(data) {
            $('.cart-table__footer strong').text(data['total'] + " руб.");
            $('span.cart_count').text(data['count']);
		},
		error: function() { alert('error');}
		});
}

function selectShipping(event) {
    target = $(event.target);
    tab_id = target.attr("href");

    type_shipping = target.attr("data-shipping");
    $('#shipping').val(type_shipping);

    target.closest(".form_part").find("input, select, textarea").prop("disabled",true);
    $(tab_id).find("input, select, textarea").prop("disabled",false);
}

$(window).on('load',function() {

	// //set events
    // $('form.filtering').on('submit', updateCatalog);
    $('body').on('submit', 'form.filtering', updateCatalog);
    $('body').on('click', '#reset_filter', resetFilter);
	// $('body').on('change', '#type_category', changeType);
    // $('.filters input').on('change', countProducts);
    
    $('.nav-link').on('click', selectShipping);


    
	// $('#base_search').on('click',baseSearch);
	// $('#base_map').on('click',baseSearchMap);

	// $('.trigger-link').on('click',vacancyInput);

	// $('#main_cats input').on('change', checkMainCat);
	// // $('.select2-selection').on('click', countProducts);
	// //activate filters
	// //2017$('#filters_form input[type=checkbox]').on('click', updateCatalog);
	// $('#sorting').on('change', updateCatalog);
	// $('button.add, button.reduce').on('click', changeCountProduct);

    $('form.checkout_form .submit-button').on('click', sendCreateOrder);
    
    $('form.sort_parameters button').on('click', updateCatalog);


	$('.search-form input').on('input', smartAjaxSearch);

	$('body').on('click', '.pagination li a', changePage);

	$('body').on('click', '.add_to_cart, .add-to-cart', addProductInCart);
	// $('.add_to_cart, .add-to-cart').on('click', addProductInCart);
	

	$('input.change-count').on('change', addProductFromCart);
    $('div.delete-item').on('click', delProductFromCart);
	
	
	$('body').on('click', '#show_more', showMore);
});

