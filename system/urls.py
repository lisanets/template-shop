from django.contrib import admin
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.views.generic import RedirectView

#  delete later
from django.views.generic import TemplateView
#


urlpatterns = [
    url(r'^favicon\.ico$',
        RedirectView.as_view(url='/static/images/favicon.ico'),
        name='favicon'),

    url(r'^admin/', admin.site.urls),

    # Прочие приложения
    url(r'^filer/', include('filer.urls')),
    url(r'^ajax_lookup/', include('ajax_select.urls'), name='ajax_lookup'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

    # delete later
    url(r'^dev_page', TemplateView.as_view(
        template_name="dev_page.html"), name='dev_page'),
    #

    # Основные приложения
    url(r'', include('apps.feedback.urls')),
    url(r'', include('apps.catalog.urls')),
    url(r'', include('apps.account.urls')),
    url(r'', include('apps.shop.urls')),
    url(r'', include('apps.pages.urls')),
    url(r'', include('apps.posts.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
