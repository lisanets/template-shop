import scandir

import re
import os
import base64
import traceback
import shutil

from django.core.management.base import BaseCommand, CommandError
from django.core.files import File
from django.contrib.auth.models import User
from lxml import etree
from slugify import slugify
from filer.models import Image

from system import settings
from apps.catalog import models as catalog_models


REGULAR_FOR_NAME_IMPORT_FILE = r'^import\.xml$'
REGULAR_FOR_NAME_OFFER_FILE = r'^offers\.xml$'


def generate_unique_slug(name, model):
    slug = slugify(name)
    count = 0
    while True:
        try:
            prod = model.objects.get(slug=slug)
            slug = slug + "_" + str(count)
            count += 1
        except Exception:
            break
    return slug


class Command(BaseCommand):
    SCAN_FILES = None
    FILE_PATH = settings.BASE_DIR + '/../imports/'
    CAT_PARENT = {}
    CAT_COUNT_UPDATE = 0
    CAT_COUNT_CREATE = 0
    PRODUCT_COUNT_CREATE = 0
    PRODUCT_COUNT_UPDATE = 0
    IMPORT_FILR = None
    NAMESPACE = ""

    def handle(self, *args, **options):
        document = self.is_exist_files()
        if not document:
            self.stderr.write("Файлы не найдены")
            return

        for e, groups in self.get_iter_items(self.NAMESPACE + "Группа"):
            self.import_group(groups)

        # Импорт товаров
        self.stdout.write("Импортирование товаров")
        count = 0
        products = self.get_iter_items(self.NAMESPACE + "Товар")
        for e, product in products:
            self.import_product(product)
            count += 1
        return
        self.import_warehouses()
        self.import_offers()

    def is_exist_files(self):
        files = scandir.scandir(self.FILE_PATH)
        self.SCAN_FILES = files
        b = False
        for s in files:
            b = True
            break
        return b

    def get_name_offers_file(self):
        for f in scandir.scandir(self.FILE_PATH):
            if re.match(REGULAR_FOR_NAME_OFFER_FILE, f.name):
                return f.name
        return False

    def get_offers_items(self):
        file_name = self.get_name_offers_file()
        if file_name:
            return etree.iterparse(self.FILE_PATH + file_name, tag=self.NAMESPACE + u"Предложение",  events=["end", ])
        else:
            return False

    def import_offer(self, offer):
        id_pr = offer.find(self.NAMESPACE + u"ИД").text
        try:
            product = catalog_models.Product.objects.get(id_1c=id_pr)
        except:
            pass

        # find price
        try:
            price = float(offer.find(self.NAMESPACE + u"Цены").find(self.NAMESPACE +
                                                                    u"Цена").find(self.NAMESPACE + u"ЦенаЗаЕдиницу").text.strip(" \n\t"))
        except:
            self.stderr.write(u"Проблема в импорте цены - " + id_pr)
            return
        if price < product.get_price():
            product.old_price = product.price
            product.count_imports = 0
        elif price > product.get_price():
            product.old_price = 0
            product.count_imports = 0
        elif product.get_price() == price and product.get_old_price() != 0:
            if catalog_models.Settings.get_count_days_discount() > product.get_count_imports():
                product.count_imports += 1
            else:
                product.old_price = 0
                product.count_imports = 0
        product.price = price

        # count product on shops
        count = 0
        product.stocks.all().delete()
        product.count = 50
        """
        sklads = offer.findall(self.NAMESPACE + "Склад")
        for sk in sklads:
            id_warehouse = sk.get("ИДСклада").strip(" \n\t")
            #get warehouse
            warehouse = catalog_models.Warehouse.objects.get(active = True, id_1c = id_warehouse)
            warehouse_count = float(sk.get("КоличествоНаСкладе"))
            count += warehouse_count
            catalog_models.StockWarehouse.objects.create(product = product, count = warehouse_count, warehouse = warehouse)
        product.count = count
        """
        # set old price

        self.stdout.write(u"Импортирована цена для - " + id_pr)
        product.save()

    def import_offers(self):
        offers = self.get_offers_items()
        if offers:
            for o in offers:
                self.import_offer(o[1])

    def get_name_import_file(self):
        for f in scandir.scandir(self.FILE_PATH):
            if re.match(REGULAR_FOR_NAME_IMPORT_FILE, f.name):
                return f.name
        return False

    def get_iter_items(self, tag):
        import_file = self.get_name_import_file()
        if import_file:
            return etree.iterparse(self.FILE_PATH + import_file, tag=tag, events=["end", ])
        else:
            return []

    def import_group(self, group, parent=None):
        try:
            id_1c = group.find(self.NAMESPACE + "ИД").text.strip(" \n\t")
            category_name = group.find(
                self.NAMESPACE + "Наименование").text.strip(" \n\t")
        except:
            self.stderr.write("Проблема с информацией в категории")
            traceback.print_exc()
            return

        if category_name != 'Интернет-магазин':
            print(id_1c, category_name, parent)

            # Создание или обновление категории товаров
            fields = ({
                'unloading_id': id_1c,
                'title': category_name,
                'slug': slugify(category_name),
                'parent': parent
            })
            category, created = catalog_models.Category.objects\
                .update_or_create(unloading_id=id_1c, defaults=fields)

            childs = group.find(self.NAMESPACE + "Группы")
            if childs is not None:
                for item in childs.findall(self.NAMESPACE + "Группа"):
                    self.import_group(item, category)

    def import_product(self, product):
        product_xml = product
        try:
            id_1c = product.find(self.NAMESPACE + "ИД").text.strip(" \n\t")
            group = product.find(
                self.NAMESPACE + "Группы").find(self.NAMESPACE + "ИД").text.strip(" \t\n")
            cat = catalog_models.Category.objects.get(unloading_id=group)

            product_name = product.find(
                self.NAMESPACE + "Наименование").text.strip(" \t\n")
            product_unit = product.find(
                self.NAMESPACE + "БазоваяЕдиница").get("НаименованиеПолное")

            product_price = product.find(
                self.NAMESPACE + "Цена2").text.strip(" \n\t")
            product_wholesale_price = product.find(
                self.NAMESPACE + "Цена").text.strip(" \n\t")
            product_count = product.find(
                self.NAMESPACE + "Остаток").text.strip(" \n\t")

            try:
                product_desc = product.find(
                    self.NAMESPACE + "Описание").text.strip(" \t\n")
            except AttributeError:
                product_desc = ""
            try:
                product_code = product.find(
                    self.NAMESPACE + "Артикул").text.strip(" \n\t")
            except AttributeError:
                product_code = ""
            try:
                product_barcode = product.find(
                    self.NAMESPACE + "Штрихкод").text.strip(" \n\t")
            except AttributeError:
                product_barcode = ""
            try:
                image_base64 = product.find(u'Картинка').find(
                    u'base64Binary').text.strip(" \t\n")
            except Exception:
                image_base64 = None
        except Exception:
            self.stderr.write("Проблема с информацией в товаре")
            traceback.print_exc()
            return
        print(id_1c, product_name, group)

        # Создание и обновление категории
        fields = ({
            'unloading_id': id_1c, 'code': product_code,
            'title': product_name, 'slug': slugify(product_name),
            'category': cat, 'price': int(product_price),
            'wholesale_price': int(product_wholesale_price),
            'old_price': 0,
            'count': int(product_count),
            'description': product_desc
        })
        product, created = catalog_models.Product.objects.update_or_create(
            unloading_id=id_1c, defaults=fields)

        self.set_attributes(product_xml, product)
        self.set_thumbnail(product, image_base64)

    def set_attributes(self, product_xml, product):
        product.product_attrbutes.all().delete()
        try:
            attrs = product_xml.find(self.NAMESPACE + "ЗначенияСвойств")\
                .findall(self.NAMESPACE + "ЗначенияСвойства")
        except Exception:
            return
        for attr in attrs:
            id_1c = attr.find(self.NAMESPACE + "ИД").text.strip(" \t\n")
            attribute_name = attr.find(self.NAMESPACE + "Наименование")\
                .text.strip(" \t\n")
            attribute_value = attr.find(self.NAMESPACE + "Значение")\
                .text.strip(" \t\n")

            group, created = catalog_models.AttributesGroup\
                .objects.update_or_create(title=attribute_name)
            value, created = catalog_models.AttributeValue\
                .objects.update_or_create(title=attribute_value)
            fields = ({'product': product, 'group': group, 'value': value})
            catalog_models.Attribute.objects.update_or_create(
                unloading_id=id_1c, defaults=fields)

    def import_warehouses(self):
        file_name = self.get_name_offers_file()
        # deactivate all warehouses
        if not file_name:
            return False
        catalog_models.Warehouse.deactivate()
        for e, w in etree.iterparse(self.FILE_PATH + file_name, tag=self.NAMESPACE + u"Склады",  events=["end", ]):
            for s in w.findall(self.NAMESPACE + "Склад"):
                id_1c = s.find(self.NAMESPACE + "ИД").text.strip(" \t\n")
                name_warehouse = s.find(
                    self.NAMESPACE + "Наименование").text.strip(" \t\n")
                # get warehouse
                try:
                    warehouse = catalog_models.Warehouse.objects.get(
                        id_1c=id_1c)
                    warehouse.title_1c = name_warehouse
                    warehouse.active = True
                    if not warehouse.is_change_title():
                        warehouse.site_title = name_warehouse
                    warehouse.save()
                except:
                    self.stdout.write("Добавлен новый склад " + id_1c)
                    catalog_models.Warehouse.objects.create(
                        id_1c=id_1c,  title_1c=name_warehouse, site_title=name_warehouse)

    def add_arguments(self, parser):
        parser.add_argument('domen')

    def set_thumbnail(self, instance, image_base64):
        if not instance.thumbnail and image_base64:
            path, name = self.get_image_base64(image_base64, instance)
            thumbnail = self.create_image(path, name)
            instance.thumbnail = thumbnail
            instance.save()
            print(instance.thumbnail)

    def set_gallery_image(self, image):
        name = image[image.rfind('/') + 1:]
        path = name
        url = image
        image = self.create_image(path, url)
        return image

    def get_image_base64(self, image_base64, product):
        path = os.path.join(settings.MEDIA_ROOT, 'products')
        try:
            os.mkdir(path)
        except FileExistsError:
            pass
        path = os.path.join(path, product.slug + '.png')
        name = product.slug + '.png'
        self.decb64img(image_base64, path)
        return path, name

    def create_image(self, path, name):
        full_path = open(path, 'rb')
        file_obj = File(full_path, name=name)
        user = User.objects.get(username='Admin')
        fields = ({'original_filename': name, 'owner': user, 'file': file_obj})
        obj, created = Image.objects.get_or_create(
            original_filename=name, defaults=fields)
        full_path.close()
        return obj

    def decb64img(self, str, path):
        file_obj = open(path, 'wb')
        file_obj.write(base64.b64decode(str))
        file_obj.close()
        return path
