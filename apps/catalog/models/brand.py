from django.db import models

from .. import models as catalog_models


class Brand(models.Model):
    active = models.BooleanField(verbose_name="Активность", default=True)
    title = models.CharField(verbose_name="Заголовок",
                             blank=False, default="", max_length=300)
    country = models.ForeignKey(
        catalog_models.Country, verbose_name="Страна",
        blank=True, null=True, related_name="brands")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "производитель"
        verbose_name_plural = "Производители"
