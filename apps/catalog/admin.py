from django import forms
from django.contrib import admin
from django_mptt_admin.admin import DjangoMpttAdmin
from mptt.admin import MPTTModelAdmin
from ajax_select.admin import AjaxSelectAdmin, AjaxSelectAdminTabularInline
from ajax_select.fields import (AutoCompleteSelectField,
                                autoselect_fields_check_can_add)

from apps.seo.admin import SeoAdminMixin
from . import models


class CategoryAdmin(SeoAdminMixin, DjangoMpttAdmin):
    fieldsets = (
        (None, {'fields': (
            'active',
            'unloading_id',
            'title',
            'slug',
            'parent',
            'thumbnail',
            'description'
        )}),
        ("SEO", {'fields': SeoAdminMixin.seo_fields}),
    )
    list_display = ('title', 'slug', 'parent')
    prepopulated_fields = {"slug": ("title",)}


class AttributeForm(forms.ModelForm):
    product = AutoCompleteSelectField('product', required=False,
                                      label="Товар")
    value = AutoCompleteSelectField('attribute_value', required=False,
                                    label="Значение")


class AttributeInline(AjaxSelectAdminTabularInline):
    model = models.Attribute
    form = AttributeForm
    extra = 0


class AttributeAdmin(AjaxSelectAdmin):
    form = AttributeForm

    def get_form(self, request, obj=None, **kwargs):
        form = super(AttributeAdmin, self).get_form(request, obj, **kwargs)
        autoselect_fields_check_can_add(form, self.model, request.user)
        return form

class NumAttributeForm(forms.ModelForm):
    product = AutoCompleteSelectField('product', required=False,
                                      label="Товар")

class NumAttributeInline(AjaxSelectAdminTabularInline):
    model = models.NumAttribute
    form = NumAttributeForm
    extra = 0

class NumAttributeAdmin(AjaxSelectAdmin):
    form = AttributeForm

    def get_form(self, request, obj=None, **kwargs):
        form = super(NumAttributeForm, self).get_form(request, obj, **kwargs)
        autoselect_fields_check_can_add(form, self.model, request.user)
        return form


class ColorInline(admin.TabularInline):
    model = models.Color
    extra = 0


class ProductForm(forms.ModelForm):
    category = AutoCompleteSelectField('categories', required=True,
                                       label="Категория")
    parent = AutoCompleteSelectField('product', required=False,
                                     label="Родитель")
    brand = AutoCompleteSelectField('brand', required=False,
                                    label="Производитель")


class ProductGalleryInline(admin.TabularInline):
    model = models.ProductGallery
    extra = 0


class ProductAdmin(MPTTModelAdmin, SeoAdminMixin):
    fieldsets = (
        (None, {'fields': ('active', 'unloading_id', 'title', 'slug',
                           'category', 'parent', 'brand', 'thumbnail')}),
        ('Основное', {'fields': ('code', 'price', 'old_price',
                                 'wholesale_price', 'count', 'new', 'hit',
                                 'sale', 'description')}),
        ("SEO", {'fields': SeoAdminMixin.seo_fields}),
    )
    list_display = ('title', 'category')
    prepopulated_fields = {"slug": ("title",)}
    form = ProductForm
    inlines = [ProductGalleryInline, AttributeInline,NumAttributeInline, ColorInline]


admin.site.register(models.Category, CategoryAdmin)
admin.site.register(models.Product, ProductAdmin)
admin.site.register(models.Country)
admin.site.register(models.Brand)
admin.site.register(models.NumAttribute)
admin.site.register(models.AttributesGroup)
admin.site.register(models.AttributeValue)
admin.site.register(models.Attribute, AttributeAdmin)
admin.site.register(models.ColorValue)
admin.site.register(models.Color)
