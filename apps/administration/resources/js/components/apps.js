$('.applications').mosaicflow({
    itemSelector: '.app',
    minItemWidth: 420,
    minColumns: 1,
    columnClass: 'applications__column'
});
