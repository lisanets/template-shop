from django import template
from django.core.exceptions import ObjectDoesNotExist

from ..models import Navigation

register = template.Library()


@register.simple_tag
def get_nav(search):
    try:
        try:
            return Navigation.objects.get(alias=search).get_children()
        except ValueError:
            return []
    except ObjectDoesNotExist:
        return []
