from django.conf.urls import url
from django.views.generic.base import TemplateView

from .views import (IndexView, PageView, OffersView, OfferView, ContactsPage)


urlpatterns = [
    # Главная страница
    url(r'^$', IndexView.as_view(), name="index"),

    # Прочие страницы
    url(r'^contacts/$', ContactsPage.as_view(), name="contacts"),
    url(r'^offers/$', OffersView.as_view(), name="offers"),
    url(r'^offer/(?P<slug>[\w-]+)/$', OfferView.as_view(), name="offer"),
    url(r'^privacy-policy/$', TemplateView.as_view(
        template_name="privacy-policy.html"), name="privacy-policy"),
    url(r'^personal-data/$', TemplateView.as_view(
        template_name="personal-data.html"), name="personal-data"),
    # Динамические страницы
    url(r'^(?P<slug>[\w-]+)/$', PageView.as_view(), name='page'),
]
