from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from ckeditor_uploader.fields import RichTextUploadingField
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField



MODES = (
    ("", "Онлайн оплата отсутствует"),
    ("https://3dsec.sberbank.ru/payment/", "Тестовый режим"),
    ("https://securepayments.sberbank.ru/payment/", "Боевой режим"),
)

class SettingsBase(models.Model):
    """Базовая модель настроек сайта"""

    # Язык сделан уникальным для того, что бы можно было сделать настройки
    # относительно системных настроек языка в django, или для мультиязычности
    language = models.CharField(choices=settings.LANGUAGES, max_length=32,
                                unique=True, verbose_name=_('Язык'))

    # Основные настройки сайта

    name = models.CharField(verbose_name="Наименование сайта", null=False,
                            blank=True, max_length=300, default="")
    full_address = models.CharField(verbose_name="Полный адрес", default="",
                                    max_length=300, blank=True)
    address = models.CharField(verbose_name="Адрес", max_length=300,
                               default="", blank=True)
    email = models.EmailField(verbose_name="Email", blank=True, null=False)
    phones = models.CharField(verbose_name="Номера телефонов",
                              help_text="разделитель ;", default="",
                              blank=True, max_length=300)
    time_work = models.CharField(verbose_name="Режим работы", max_length=300,
                                 default="", blank=True)
    coord_x = models.FloatField(verbose_name="Координата X на карте",
                                blank=True, null=True)
    coord_y = models.FloatField(verbose_name="Координата Y на карте",
                                blank=True, null=True)
    price_list = FilerFileField(
        verbose_name="Прайс лист", blank=True, null=True,
        related_name="catalog_company", on_delete=models.SET_NULL,
        help_text="Узнайте о нас больше")
    privacy_policy = RichTextUploadingField(
        verbose_name="Политика конфиденциальности", default="", blank=True)
    personal_data = RichTextUploadingField(
        verbose_name="Согласие на обработку персональных данных", default="", blank=True)

    color_scheme = models.CharField(
        verbose_name="Цветовая схема сайта", default="#007bff", max_length=7)

    # Социальные сети

    vkontakte = models.CharField(verbose_name="Ссылка на ВК группу",
                                 max_length=300, blank=True)
    facebook = models.CharField(verbose_name="Ссылка на fb группу",
                                max_length=300, blank=True)
    instagram = models.CharField(verbose_name="Ссылка на instagram",
                                 max_length=300, blank=True)
    telegram = models.CharField(verbose_name="Ссылка на telegram",
                                max_length=300, blank=True)
    twitter = models.CharField(verbose_name="Ссылка на twitter",
                               max_length=300, blank=True)
    youtube = models.CharField(verbose_name="Ссылка на youtube",
                               max_length=300, blank=True)

    # Настройки SEO

    seo_text = RichTextUploadingField(verbose_name="SEO текст", default="",
                                      blank=True)
    seo_img = FilerImageField(verbose_name="SEO картинка", null=True,
                              blank=True, related_name="seo_img",
                              on_delete=models.SET_NULL)
    meta_title = models.CharField(verbose_name="SEO Заголовок", max_length=300,
                                  null=False, blank=True, default="")
    meta_description = models.TextField(verbose_name="Meta Description",
                                        default="", blank=True)
    meta_template_description = models.TextField(
        verbose_name="Meta template Description", default="", blank=True)
    meta_keywords = models.TextField(verbose_name="Meta Keywords", default="",
                                     help_text="вводить через запятую",
                                     blank=True)
    meta_template_title = models.CharField(
        verbose_name="Шаблон для сайта", default="", blank=True,
        max_length=300, help_text=""" ||site|| - имя сайта,
        ||object|| - имя объекта """)
    robots_txt = models.TextField(verbose_name="robots.txt", default="",
                                  blank=True)
    head_scripts = models.TextField(verbose_name="Вывод в head", default="",
                                    blank=True)
    scripts = models.TextField(verbose_name="Скрипты под footer", default="",
                               blank=True)

    # Сбербанк эквайринг

    mode_payment = models.CharField(verbose_name="Режим оплаты", choices=MODES,
                                 default="", max_length=100, blank = True)
    shop_id = models.CharField(verbose_name="Логин api", blank = True,
                                 default="", max_length=100)
    api_key = models.CharField(verbose_name="Ключ api", blank = True,
                                 default="", max_length=100)


    def get_phones(self):
        if self.phones:
            return self.phones.split(';')
        return ''

    def get_phone(self):
        if self.get_phones():
            return self.get_phones()[0]
        return ''

    def get_coord_x(self):
        return str(self.coord_x).replace(",", ".")

    def get_coord_y(self):
        return str(self.coord_y).replace(",", ".")

    @staticmethod
    def get_settings():
        return Settings.objects.filter(language=settings.LANGUAGE_CODE).first()

    def __str__(self):
        return str(dict(settings.LANGUAGES).get(self.language, self.language))

    class Meta:
        abstract = True
        verbose_name = _('Настройки сайта')
        verbose_name_plural = _('Настройки сайта')
        ordering = ['language']


class Settings(SettingsBase):
    pass
