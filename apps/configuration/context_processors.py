from django.db.models import Q

from apps.catalog.models import Category
from apps.nav.models import Navigation
from .models import Settings


def website_settings(request):
    data = {}

    web_settings = Settings.objects.filter(
        language=request.LANGUAGE_CODE).first()
    if not web_settings:
        web_settings = Settings(language='fallback')

    data['categories'] = Category.objects.filter(parent=None, active=True)
    data['header_menu'] = Navigation.objects.filter(parent=None)

    data['SERVER'] = request
    data['SERVER_DICT'] = request.__dict__
    data['SERVER_GET'] = request.GET
    data['SERVER_META'] = request.META
    data['SERVER_SESSION'] = request.session
    data['SERVER_SESSION_DICT'] = request.session.__dict__

    data['settings'] = web_settings

    return data
