import requests
from django.core.urlresolvers import reverse
from apps.configuration.models import Settings
import json

class BankPayment:

    def __init__(self, order, request):
        super(BankPayment, self).__init__()

        settings = Settings.objects.filter(
            language=request.LANGUAGE_CODE).first()
        if not settings:
            settings = Settings(language='fallback')

        return_url = "{0}://{1}/{2}".format(request.META['REQUEST_SCHEME'],request.META['HTTP_HOST'],reverse("validate-payment"))

        self.area = settings.mode_payment
        self.shop_id = settings.shop_id
        self.api_key = settings.api_key
        self.order = order
        self.return_url = return_url

    def is_valid(self):

        if self.area and self.shop_id and self.api_key and self.order and self.return_url:
            return True
        return False

    def register_payment(self):
        """ Регистрируем заказ в системе сбербанка 

        amount - Сумма заказа задается в копейках

        returnUrl - Ссылка для перенаправления после оплаты
        """

        DO = "register.do"

        data = {'userName':self.shop_id, 'password':self.api_key, 'orderNumber':self.order.id,
            'amount':int(self.order.total*100), 'returnUrl' : self.return_url}

        resp = requests.post(self.area + "rest/" + DO, data = data).text

        response_data = json.loads(resp)

        bank_id = response_data['orderId']
        self.order.bank_id = bank_id
        self.order.save()

        redirect = response_data['formUrl']

        if not bank_id or not redirect:
            return ""

        return redirect


    def set_status_order(self):
        """ Проверяем оплату заказа и ставим статус """

        DO = "getOrderStatus.do"
        data = {'userName':self.shop_id, 'password':self.api_key, 'orderId':self.order.id}
        response_data = json.loads(requests.post(self.area + "rest/" + DO, data = data).text)
        if response_data.get('OrderStatus', False) and response_data['OrderStatus'] == 2:
            self.order.payment = True
            self.order.save()
            return True
        return False




