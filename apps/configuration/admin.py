from django.contrib import admin
from .models import Settings, Slider


class SliderInline(admin.TabularInline):
    model = Slider
    extra = 1


class SettingsAdmin(admin.ModelAdmin):
    list_display = ('language', 'name')
    fieldsets = (
        (None, {'fields': (
            'language',
            'name',
            'color_scheme'
        )}),
        ("Контакты", {'fields': (
            'full_address',
            'address',
            'phones',
            'email',
            'time_work',
            ('coord_x', 'coord_y'),
            'price_list',
            'privacy_policy',
            'personal_data'
        )}),
        ("Социальные сети", {'fields': (
            'vkontakte',
            'facebook',
            'instagram',
            'telegram',
            'twitter',
            'youtube'
        )}),
        ("SEO", {'fields': (
            'seo_text',
            'seo_img',
            'meta_title',
            'meta_template_title',
            'meta_description',
            'meta_template_description',
            'meta_keywords',
            'head_scripts',
            'scripts',
            'robots_txt'
        )}),
        ("Оплата", {'fields': (
            'mode_payment',
            'shop_id',
            'api_key',
        )})
    )
    inlines = [SliderInline]


admin.site.register(Settings, SettingsAdmin)
