from django.core.exceptions import ObjectDoesNotExist

from apps.account.models import Account
from .models import Cart, Favorites, Compare


def cart(request):
    """Добавление в контекст корзины товаров.

    Arguments:
        request {object} -- Запрос на сервер

    Returns:
        dict -- Словарь с данными по корзине
    """

    data = dict()
    if request.user.is_authenticated():
        data['auth'] = True
        data['user'] = request.user
        try:
            data['account'] = Account.objects.get(user=request.user)
            data['cart'] = Cart.objects.get(account__user=request.user)
            data['favorites'] = Favorites.objects.get(
                account__user=request.user)
            data['compare'] = Compare.objects.get(
                account__user=request.user)
        except ObjectDoesNotExist:
            data['cart'] = None
            data['favorites'] = None
            data['compare'] = None

    else:
        data['account'] = None
        data['auth'] = False
        try:
            data['cart'] = request.session['cart']
        except KeyError:
            data['cart'] = None
        try:
            data['favorites'] = request.session['favorites']
        except KeyError:
            data['favorites'] = None
        
        try:
            data['compare'] = request.session['favorites']
        except KeyError:
            data['compare'] = None
    return data
