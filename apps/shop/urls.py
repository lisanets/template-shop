from django.conf.urls import url

from .views import (
    CartTemplate, CartAddView, CartUpdateView, CartDeleteView, OrderView,
    OrderCreate, FavoritesView, AddFavoritesView, DeleteFavoritesView,
    RestoreFavoritesView, AddWaitingFavoritesView, PaymentRedirectView,CompareView,AddCompareView,DeleteCompareView,RestoreCompareView
)


urlpatterns = [
    url(r'^shop/cart/$', CartTemplate.as_view(), name='cart'),
    url(r'^shop/order/$', OrderView.as_view(), name='order'),
    url(r'^shop/favorites/$', FavoritesView.as_view(), name='favorites'),
    url(r'^shop/compare/$', CompareView.as_view(), name='compare'),
    url(r'^shop/validate-payment/$', PaymentRedirectView.as_view(), name='validate-payment'),

    # Ajax обработчики

    url(r'^api/shop/cart/add/$', CartAddView.as_view(), name='cart-add'),
    url(r'^api/shop/cart/update/$', CartUpdateView.as_view(),
        name='cart-update'),
    url(r'^api/shop/cart/delete/$', CartDeleteView.as_view(),
        name='cart-delete'),
    url(r'^api/shop/order/create/$', OrderCreate.as_view(),
        name='order-create'),
    url(r'^api/shop/favorites/add$', AddFavoritesView.as_view(),
        name='favorites-add'),
    url(r'^api/shop/favorites/delete$', DeleteFavoritesView.as_view(),
        name='favorites-delete'),
    url(r'^api/shop/favorites/restore$', RestoreFavoritesView.as_view(),
        name='favorites-restore'),
    url(r'^api/shop/favorites/add-waiting$',
        AddWaitingFavoritesView.as_view(),
        name='favorites-add-waiting'),
    url(r'^api/shop/compare/add$', AddCompareView.as_view(),
        name='compare-add'),
    url(r'^api/shop/compare/delete$', DeleteCompareView.as_view(),
        name='compare-delete'),
    url(r'^api/shop/compare/restore$', RestoreCompareView.as_view(),
        name='compare-restore'),
]
