from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.catalog.models import Product, Color
from apps.account.models import Account


class Cart(models.Model):
    """Модель корзины пользователя личного кабинета"""

    account = models.OneToOneField(
        Account, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.account.user.username

    def count(self):
        return self.items().count()

    def items(self):
        return self.cart_items.all()

    def total(self):
        total = float()
        for item in self.items():
            total += float(item.total())
        return '{0:.0f}'.format(total)

    class Meta:
        verbose_name = 'корзина'
        verbose_name_plural = 'Корзины'


class CartItem(models.Model):
    """Модель элемента корзины пользователя личного кабинета"""

    cart = models.ForeignKey(Cart, related_name="cart_items")
    product = models.ForeignKey(Product, verbose_name="Товар")
    color = models.ForeignKey(Color, verbose_name="Цвет",
                              blank=True, null=True)
    count = models.IntegerField(verbose_name="Количество", default=0)

    def price(self):
        if self.color:
            return '{0:.0f}'.format(self.color.price)
        return '{0:.0f}'.format(self.product.price)

    def total(self):
        if self.color:
            return '{0:.0f}'.format(self.color.price * self.count)
        return '{0:.0f}'.format(self.product.price * self.count)

    class Meta:
        verbose_name = 'элемент корзины'
        verbose_name_plural = 'элементы корзины'
        ordering = ['id']


@receiver(post_save, sender=Account)
def create_cart(sender, instance, created, **kwargs):
    """Создание новой модели корзины пользователя личного кабинета.

    Корзина создается по сигналу сохраненного пользователя личного кабинета,
    и привязывается к созданному пользователю личного кабинета. Корзина
    создается только в том случае если профиль был создан, а не просто
    сохранен.

    Arguments:
        sender {Account} -- Модель пользователя личного кабинета
        instance {Cart} -- Экземпляр пользователя личного кабинета
        created {bool} -- Показывает был ли создан профиль пользователя
    """

    if created:
        Cart.objects.create(account=instance)


@receiver(post_save, sender=Account)
def save_cart(sender, instance, **kwargs):
    """Сохранение модели уже существующей корзины пользователя личного кабинета.

    Корзина сохраняется по сигнялу сохранения пользователя личного кабинета.

    Arguments:
        sender {Account} -- Модель пользователя личного кабинета
        instance {Cart} -- Экземпляр пользователя личного кабинета
    """

    instance.cart.save()


class UnauthCart:
    """Корзина неавторизованного пользователя"""

    def __init__(self, cart_items=list()):
        self.cart_items = cart_items

    def items(self):
        return self.cart_items

    def count(self):
        return len(self.items())

    def total(self):
        total = float()
        cart_items = self.items()
        for item in cart_items:
            total += float(item.total())
        return '{0:.0f}'.format(total)


class UnauthCartItem:
    """Элемент корзины неавторизованного пользователя."""

    def __init__(self, product=None, color=None, count=None):
        self.product = product
        self.color = color
        self.count = count

    def price(self):
        if self.color:
            return '{0:.0f}'.format(self.color.price)
        return '{0:.0f}'.format(self.product.price)

    def total(self):
        if self.color:
            return '{0:.0f}'.format(self.color.price * self.count)
        return '{0:.0f}'.format(self.product.price * self.count)
