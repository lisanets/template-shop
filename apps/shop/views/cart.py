from django.http import JsonResponse
from django.views.generic import View, TemplateView
from django.template.loader import get_template

from apps.catalog.models import Product, Color
from ..models import Cart, CartItem, UnauthCart, UnauthCartItem


class CartTemplate(TemplateView):
    template_name = 'shop/cart.html'


class CartAddView(View):
    """
    Обработчик добавления товаров и опций в корзину.
    """

    def post(self, request):
        post_data = request.POST
        product = Product.objects.get(id=int(post_data['product-id']))
        count = int(post_data['product-count'])
        colors = product.colors.all()

        try:
            color = Color.objects.get(id=int(post_data['product-color']))
        except KeyError:
            color = None
            if colors:
                return JsonResponse({
                    'errors': True, 'message': 'Выберите цвет'})

        if request.user.is_authenticated():
            cart = self.create_auth_cart(request, product, color, count)
        else:
            cart = self.create_unauth_cart(request, product, color, count)

        return JsonResponse({
            'count': cart.count(), 'dropdown': render_dropdown(cart)})

    def create_auth_cart(self, request, product, color, count):
        """Создание корзины товаров авторизованного пользователя.

        Arguments:
            request {DjangoRequest} -- Запрос на сервер
            product {Product} -- Экземпляр товара
            color {Color} -- Экземпляр опции цвета товара
            count {int} -- Количество товаров

        Returns:
            Cart -- Корзина товаров
        """

        cart = Cart.objects.get(account__user=request.user)
        fields = {'count': count}
        cart_item, created = CartItem.objects.get_or_create(
            cart=cart, product=product, color=color, defaults=fields)
        if not created:
            cart_item.count += count
            cart_item.save()
        return cart

    def create_unauth_cart(self, request, product, color, count):
        """Создание корзины товаров неавторизованного пользователя.

        Arguments:
            request {DjangoRequest} -- Запрос на сервер
            product {Product} -- Экземпляр товара
            color {Color} -- Экземпляр опции цвета товара
            count {int} -- Количество товаров

        Returns:
            UnauthCart -- Корзина товаров
        """

        cart = request.session.get('cart', None)
        if cart:
            if cart.count():
                found_item = False
                for key, item in enumerate(cart.cart_items):
                    if item.product == product and item.color == color:
                        found_item = True
                        cart.cart_items[key].count = item.count + count
                if not found_item:
                    cart.cart_items.append(UnauthCartItem(
                        product=product, color=color, count=count))
            else:
                cart.cart_items = [UnauthCartItem(
                    product=product, color=color, count=count)]
        else:
            cart = UnauthCart(cart_items=[UnauthCartItem(
                product=product, color=color, count=count)])
        request.session['cart'] = cart
        request.session.save()
        return cart


class CartUpdateView(View):
    """Обработчик обновление количества товаров и их суммы в корзине.

    Arguments:
        View {object} -- Базовый класс представлений Django.

    Returns:
        dict -- Возвращаяет данные корзины в виде json.
    """

    def post(self, request):
        post_data = request.POST
        count = int(post_data['product-count'])

        product = Product.objects.get(id=post_data['product-id'])
        try:
            color = Color.objects.get(id=int(post_data['product-color']))
        except KeyError:
            color = None

        if request.user.is_authenticated():
            cart, cart_item = self.update_auth_cart_item(
                request, product, color, count)
        else:
            cart, cart_item = self.update_unauth_cart_item(
                request, product, color, count)
        return JsonResponse({
            'count': cart.count(),
            'price': cart_item.total(),
            'total': cart.total(),
            'dropdown': render_dropdown(cart)})

    def update_auth_cart_item(self, request, product, color, count):
        """Обновление элемента в корзине авторизованного пользователя.

        Arguments:
            request {object} -- Запрос на сервер
            product {Product} -- Товар
            color {Color} -- Опция цвета
            count {int} -- Количесто товаров

        Returns:
            (Cart, CartItem) -- Корзина товаров и измененный
            элемент корзины
        """

        cart = Cart.objects.get(account__user=request.user)
        cart_item = CartItem.objects.get(
            cart=cart, product=product, color=color)
        cart_item.count = count
        cart_item.save()
        return cart, cart_item

    def update_unauth_cart_item(self, request, product, color, count):
        """Обновление элемента в корзине неавторизованного пользователя.

        Arguments:
            request {object} -- Запрос на сервер
            product {Product} -- Товар
            color {Color} -- Опция цвета
            count {int} -- Количесто товаров

        Returns:
            (UnauthCart, UnauthCartItem) -- Корзина товаров и измененный
            элемент корзины
        """

        cart = request.session['cart']
        for key, item in enumerate(cart.cart_items):
            if item.product == product and item.color == color:
                cart.cart_items[key].count = count
                cart_item = cart.cart_items[key]
        request.session['cart'] = cart
        request.session.save()
        return cart, cart_item


class CartDeleteView(View):

    def post(self, request):
        post_data = request.POST
        product = Product.objects.get(id=post_data['product-id'])
        try:
            color = Color.objects.get(id=int(post_data['product-color']))
        except KeyError:
            color = None

        if request.user.is_authenticated():
            cart = self.delete_auth_cart_item(request, product, color)
        else:
            cart = self.delete_unauth_cart_item(request, product, color)

        response = {'count': cart.count(), 'total': cart.total(),
                    'dropdown': render_dropdown(cart)}

        if response['count'] < 1:
            response['empty'] = get_template(
                template_name='shop/includes/cart-empty.html').render()

        return JsonResponse(response)

    def delete_auth_cart_item(self, request, product, color):
        """Удаление элемента из корзины авторизованного пользователя

        Arguments:
            request {object} -- Запрос на сервер
            product {Product} -- Товар
            color {Color} -- Опция цвета

        Returns:
            (Cart, CartItem) -- Корзина товаров
        """

        cart = Cart.objects.get(account__user=request.user)
        CartItem.objects.get(
            cart=cart, product=product, color=color).delete()
        return cart

    def delete_unauth_cart_item(self, request, product, color):
        """Удаление элемента из корзины неавторизованного пользователя

        Arguments:
            request {object} -- Запрос на сервер
            product {Product} -- Товар
            color {Color} -- Опция цвета

        Returns:
            (Cart, CartItem) -- Корзина товаров
        """

        cart = request.session['cart']
        for key, item in enumerate(cart.cart_items):
            if item.product == product and item.color == color:
                del cart.cart_items[key]
        request.session['cart'] = cart
        request.session.save()
        return cart


def render_dropdown(cart):
    """Рендер выпадающее меню на основе корзины товаров.

    Arguments:
        cart {Cart|UnauthCart} -- Корзина товаров

    Returns:
        str -- HTML рендер в виде строки
    """

    return get_template(template_name='shop/includes/cart-dropdown.html')\
        .render({'add_product': True, 'cart': cart})
