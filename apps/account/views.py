from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.shortcuts import redirect as django_redirect
from django.views.generic import TemplateView, ListView, View

from apps.feedback.models import Subscriber
from apps.shop.models import Order, Favorites, FavoritesItem
from .models import Account
from .forms import LoginForm, RegisterForm, ChangePasswordForm


# Представления на странцы пользователя личного кабинета

class OrdersList(ListView):
    """Страница со списком заказаов в личном кабинете"""

    model = Order
    context_object_name = "orders"
    template_name = "account/orders.html"

    def get_queryset(self):
        return Order.objects.filter(
            account__user=self.request.user).order_by("-id")


class DataTemplate(TemplateView):
    """Страница с данными о пользователе в личном кабинете"""

    template_name = "account/data.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            context["account"] = Account.objects.get(user=self.request.user)
        return context

    def render_to_response(self, context, **response_kwargs):
        # Закрытие доступа к личному кабинету для не авторизованных
        # пользователей
        if not self.request.user.is_authenticated():
            return django_redirect('/')
        return super().render_to_response(context, **response_kwargs)


class FavoritesTemplate(TemplateView):
    """Странца с избранными товарами личного кабинета"""

    template_name = "account/favorites.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["favorites_page"] = True
        return context


class FavoritesWaitingList(ListView):
    """
    Страница со списком ожидаемых товаров относительно избранных товаров
    личного кабинета
    """

    model = FavoritesItem
    context_object_name = "waiting"
    template_name = "account/waiting.html"

    def get_queryset(self):
        favorites = Favorites.objects.get(account__user=self.request.user)
        return self.model.objects.filter(favorites=favorites, waiting=True)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["favorites_page"] = True
        return context


class PasswordTemplate(TemplateView):
    """Страница смены пароля в личном кабинете"""

    template_name = "account/password.html"


class SubscribeTemplate(TemplateView):
    """Страница подписки в личном кабинете"""

    template_name = "account/subscribe.html"


# Ajax обработчики

class LoginView(View):
    """Обработчик запроса на вход в личный кабинет"""

    def post(self, request):
        form = LoginForm(request.POST)
        if form.is_valid():
            redirect = reverse("account-orders")
            email = form.cleaned_data["email"]
            password = form.cleaned_data["password"]
            auth = Account.auth_with_password(request, email, password)
            if not auth:
                try:
                    account = Account.objects.get(email=email)
                    return JsonResponse({
                        "errors": True,
                        "fields": {
                            "password": "Неверный пароль"
                        },
                        "message": "Неверный email или пароль"
                    })
                except:
                    return JsonResponse({
                        "errors": True,
                        "fields": {
                            "email": "Неверный email",
                            "password": "Неверный пароль"
                        },
                        "message": "Неверный email или пароль"
                    })
            return JsonResponse({"errors": False, "redirect": redirect})
        return JsonResponse({
            "errors": True,
            "fields": form.errors,
            "message": "Введите правильный адрес электронной почты."
        })


class RegisterView(View):
    """Регистрация пользователя в личном кабинете"""

    def post(self, request):
        form = RegisterForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            user = Account.register(email)
            if not user:
                return JsonResponse({
                    "errors": True,
                    "fields": {
                        'email': "Пользователь с таким email уже существует"
                    }
                })
            Account.auth(request, user)
            return JsonResponse({"errors": False, "reload": True})
        return JsonResponse({
            "errors": True,
            "fields": form.errors,
            "message": "Введите правильный адрес электронной почты."
        })


class ChangeDataView(View):
    """Обработчик изменения данных профиля пользователя"""

    def post(self, request, *args, **kwargs):
        error_list = {}
        fields = request.POST

        if fields["name"] == "":
            error_list["name"] = "Укажите имя"
        if fields["phone"] == "":
            error_list["phone"] = "Укажите телефон"
        if fields["email"] == "":
            error_list["email"] = "Укажите email"

        if not error_list:
            account = Account.objects.get(user=request.user)
            account.name = fields["name"]
            account.phone = fields["phone"]
            account.email = fields["email"]
            account.save()
            return JsonResponse({
                "errors": False,
                "message": "Сообщение"
            })
        else:
            return JsonResponse({"errors": True, "fields": error_list})


class SubscribeAddView(View):
    """Обработчик добавления пользователя личного кабинета в подписчики"""

    def post(self, request, *args, **kwargs):
        try:
            Subscriber.objects.get(user=request.user).delete()
            return JsonResponse({"errors": False, "message": "Вы отписались"})
        except ObjectDoesNotExist:
            Subscriber.objects.create(user=request.user)
            return JsonResponse({"errors": False, "message": "Вы подписались"})


class ChangePasswordView(View):
    """
    Обработчик смены пароля пользоватекля Django к которому привязан личный
    кабинет.
    """

    def post(self, request):
        account = Account.objects.get(user=request.user)
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            old_password = form.cleaned_data["old_password"]
            if not account.check_password(old_password):
                return JsonResponse({
                    "errors": True,
                    "fields": {"old_password": "Старый пароль введён неверно"}
                })
            account.user.set_password(form.cleaned_data["password"])
            account.user.save()
            Account.auth(request, account.user)
            return JsonResponse({"errors": False})
        # Возврат ошибок, если форма не прошла валидацию
        return JsonResponse({"errors": True, "fields": form.errors})
