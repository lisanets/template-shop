from django.conf.urls import url

from .views import (
    OrdersList, DataTemplate, FavoritesTemplate, FavoritesWaitingList,
    PasswordTemplate, SubscribeTemplate, LoginView, RegisterView,
    ChangeDataView, SubscribeAddView, ChangePasswordView
)

urlpatterns = [
    url(r'^account/orders/$', OrdersList.as_view(), name='account-orders'),
    url(r'^account/data/$', DataTemplate.as_view(), name='account-data'),
    url(r'^account/favorites/$', FavoritesTemplate.as_view(),
        name='account-favorites'),
    url(r'^account/waiting/$', FavoritesWaitingList.as_view(),
        name='account-waiting'),
    url(r'^account/password/$', PasswordTemplate.as_view(),
        name='account-password'),
    url(r'^account/subscribe/$', SubscribeTemplate.as_view(),
        name='account-subscribe'),

    # Ajax обработчики

    url(r'^api/account/login/$', LoginView.as_view(),
        name='account-login'),
    url(r'^api/account/register/$', RegisterView.as_view(),
        name='account-register'),
    url(r'^api/account/data/change/$', ChangeDataView.as_view(),
        name='account-change-data'),
    url(r'api/account/subscribe/add/', SubscribeAddView.as_view(),
        name='account-subscribe-add'),
    url(r'^api/account/password/change/$', ChangePasswordView.as_view(),
        name='account-change-password'),
]
